import styled from 'styled-components';
import { colors } from './util/constants'
import { Flex } from './components'
import media from './util/media'

export const Builder = styled.div`
  max-width: 600px;
  background: white;
  margin: 16px;
  width: 100%;
  border-radius: 6px;
  border: 2px solid ${colors.blue_200};
  ${media.phonePlus`
    margin: 48px;
  `}
`

export const Header = styled.div`
  background: ${colors.blue_100};
  border-bottom: 2px solid ${colors.blue_200};
  padding: 10px 24px;
`

export const Label = styled.div`
  padding: 16px 0;
  min-width: 100%;
  ${media.phonePlus`
    min-width: 200px;
    padding-top: 8px;
  `}
`

export const Row = styled(Flex)`
  padding: 0 0 24px 0;
  align-items: flex-start;
  justify-content: flex-start;
  flex-wrap: wrap;
  ${media.tablet`
    flex-wrap: nowrap;
  `}
`

export const Output = styled.div`
  background: ${colors.gray_100};
  padding: 24px;
  border-radius: 6px;
  margin: 48px;
  width: 100%;
  max-width: 600px;
  display: none;
  ${media.tabletPlus`
    display: block;
  `}
`
