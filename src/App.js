import React, { useState, useEffect } from 'react';
import services from './util/services'
import { Button, Flex, Text, Input, Checkbox, Select, Choices } from './components/index'
import { Builder, Header, Label, Row, Output } from './styled'
import { weight, colors } from './util/constants'

function App() {
  const [field, setField] = useState(services.getField)
  const [error, setError] = useState()
  const [output, setOutput] = useState()

  //Recover State of field if window was closed
  useEffect(() => {
    const recoveredState = services.recoverState()
    if(recoveredState) setField(recoveredState)
  }, [])

  //Field Functions
  function updateField(e){
    field[e.target.name] = e.target.value
    setField(field)
    services.saveState(JSON.stringify(field))
  }

  function toggleRequired(){
    field.required = !field.required
    setField(field)
    services.saveState(JSON.stringify(field))
  }

  function updateChoices(choices){
    field.choices = choices
    setField(field)
    services.saveState(JSON.stringify(field))
  }

  //Button Actions
  async function saveField(){
    if(!field.label){
      setError('Please enter a label')
    }
    else {
      setError(undefined)
      if(field.default !== "" && !field.choices.includes(field.default)) field.choices.push(field.default)
      setOutput(JSON.stringify(field))
      services.saveState(JSON.stringify(field))
      const result = await services.saveField(field)
      alert(result.data.status)
    }
  }

  function cancel(){
    setError(undefined)
    setField({displayAlpha: true, choices: []})
    services.clearState()
  }

  return (
    <Flex justify="space-between">
      <Builder>
        <Header>
          <Text kind="h2" color={colors.blue_300}>Field Builder</Text>
        </Header>

        <Flex column padding="24px">
          {/* LABEL */}
          <Row>
            <Label>
              <Text>Label</Text>
            </Label>

            <Input
              initialvalue={field.label}
              name="label"
              change={updateField}
              placeholder="(e.g. Sales Region)"
              detect={field}/>
          </Row>

          {/* TYPE */}
          <Row>
            <Label>
              <Text>Type</Text>
            </Label>

            <Flex align="center">
              <Text padding="0 8px 0 0">Multi-select</Text>
              <Checkbox
                onClick={toggleRequired}
                selected={field.required}
                detect={field}/>
              <Text>A value is required</Text>
            </Flex>
          </Row>

          {/* DEFAULT VALUE */}
          <Row>
            <Label>
              <Text>Default Value</Text>
            </Label>

            <Input
              initialvalue={field.default}
              name="default"
              change={updateField}
              placeholder="(e.g. Asia)"
              detect={field}/>
          </Row>

          {/* CHOICES */}
          <Row>
            <Label>
              <Text>Choices</Text>
            </Label>

            <Choices
              initialvalue={field.choices}
              change={updateChoices}
              detect={field}
            />
          </Row>

          {/* ORDER */}
          <Row>
            <Label>
              <Text>Order</Text>
            </Label>

            <Select
              initialvalue={field.displayAlpha ? services.orderOptions[0] : services.orderOptions[1]}
              detect={field}
              change={e => updateField({target: {name: 'displayAlpha', value: e.value}})}
              options={services.orderOptions}
              />
          </Row>

          {error && (
            <Row justify="center">
              <Text color={colors.red}>{error}</Text>
            </Row>
          )}

          {/* FOOTER */}
          <Flex align="center" justify="center">
            <Button onClick={saveField} kind="primary">Save Changes</Button>
            <Text padding="0 0 0 16px">or</Text>
            <Button onClick={cancel} kind="warning">Cancel</Button>
          </Flex>
        </Flex>
      </Builder>

      <Output>
        <Text size="18px">Output</Text>
        <Text>{output}</Text>
      </Output>
    </Flex>
  );
}

export default App;
