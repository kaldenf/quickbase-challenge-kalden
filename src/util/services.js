import axios from 'axios'

const FieldService =  {
	getField: function(id) {
		return {
		  "label": "Sales region",
		  "required": false,
		  "choices": [
  			"Asia",
  			"Australia",
  			"Western Europe",
  			"North America",
  			"Eastern Europe",
  			"Latin America",
  			"Middle East and Africa"
		  ],
		  "displayAlpha": true,
		  "default": "North America"
		}
	},
  orderOptions: [
    {label: 'Display choices in Alphabetical', value: true},
    {label: 'Dont Display choices in Alphabetical', value: false}
  ],
	saveField: function (fieldJson) {
    console.log(fieldJson)
    return axios.post('http://www.mocky.io/v2/566061f21200008e3aabd919', fieldJson)
	},
  saveState: function(fieldJson) {
    try {
      localStorage.setItem('field', fieldJson)
    } catch {
      console.log('error saving field in localStorage')
    }
  },
  recoverState: function() {
    try {
      const field = localStorage.getItem('field')
      return JSON.parse(field)
    } catch {
      console.log('error getting field in localStorage')
      return {}
    }
  },
  clearState: function (){
    try {
      localStorage.removeItem('field')
    } catch {
      console.log('localstorage not enabled')
    }

  }
}

export default FieldService
