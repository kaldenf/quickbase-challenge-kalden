import { css } from 'styled-components';

const sizes = {
  phonePlus: '500px',
  tablet: '800px',
  tabletPlus: '1024px',
};

export default Object.keys(sizes).reduce((acc, label) => {
  acc[label] = (...args) => css`
    @media ${label === 'laptop' && 'screen and '} (min-width: ${sizes[label]}) {
      ${css(...args)};
    }
  `;
  return acc;
}, {});
