export const colors = {
  green: '#5CB85B',
  red: '#FF0100',
  blue_100: '#D9EDF7',
  blue_200: '#C5EBF3',
  blue_300: '#297298',
  gray_100: '#CCC',
  gray_200: '#DDD'
}

export const weight = {
  thin: 200,
  light: 300,
  medium: 500,
  bold: 700,
  black: 800
}
