import React, { useEffect, useState } from 'react'
import styled from "styled-components"
import { colors } from '../util/constants'
import { Text, Flex, Button } from './'
import PropTypes from 'prop-types'

const Items = styled.div`
  width: 100%;
  padding: 8px;
  border: 1px solid ${colors.gray_100};
  border-radius: 6px;
`

const Item = styled.div`
  padding: 4px 8px;
  position: relative;
  &:hover {
    background: ${colors.gray_200};
    cursor: pointer;
    &:after {
      content: '';
      background: url('./icon-close.svg');
      background-size: contain;
      background-repeat: no-repeat;
      width: 20px;
      height: 20px;
      position: absolute;
      top: 7px;
      right: 5px;
    }
  }

`

const Add = styled.input`
  padding: 10px 15px;
  width: 100%;
  border: 1px solid ${colors.gray_100};
  background: none;
  font-size: 16px;
  border-radius: 6px;
  margin-right: 8px;
`

const Choices = (props) => {
  const [choices, setChoices] = useState(props.initialvalue ? props.initialvalue : [])
  const [newChoice, updateNewChoice] = useState('')
  const [error, setError] = useState()

  useEffect(() => {
    setChoices(props.initialvalue)
  }, [props.detect])

  function removeChoice(i){
    const remaining = choices.filter((choice, index) => index !== i)
    props.change(remaining)
    setChoices(remaining)
  }

  function addChoice(e){
    e.preventDefault()
    if(newChoice.length == 0){
      setError(`Please enter a value`)
    }
    else if(newChoice.length > 40) {
      setError(`Choice can't exceed 40 characters`)
    }
    else if (choices.length > 49){
      setError(`Limited to 50 options`)
    }
    else if (choices.includes(newChoice)){
      setError(`Cannot have duplicate options`)
    }
    else {
      choices.push(newChoice)
      setChoices(choices)
      updateNewChoice('')
      props.change(choices)
      if(error) setError(undefined)
    }
  }

  function validateChoice(e){
    if(e.target.value.length > 40) {
      setError('Choice cannot exceed 40 characters')
    } else {
      updateNewChoice(e.target.value)
      if(error) setError(undefined)
    }
  }

  return (
    <Flex column>
      <Items>
        {choices && choices.map((choice, index) => (
          <Item key={index} onClick={() => removeChoice(index)}><Text>{choice}</Text></Item>
        ))}

        <form onSubmit={addChoice}>
          <Flex padding="16px 0 0" align="center">
            <Add placeholder="New Choice" value={newChoice} onChange={validateChoice}/>
            <Button onClick={addChoice}>Add</Button>
          </Flex>
        </form>
      </Items>

      {error && (
        <Text padding="4px 0 0 4px" color={colors.red}>{error}</Text>
      )}
    </Flex>
  )
}

Choices.propTypes = {
  initialvalue: PropTypes.array,
  change: PropTypes.func
};

export default Choices
