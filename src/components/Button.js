import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { colors, weight } from '../util/constants'

const Main = styled.button`
  padding: 10px 15px;
  border: none;
  background: none;
  font-size: 16px;
  font-weight: ${weight.bold};
  cursor: pointer;
  &.primary {
    color: white;
    background: ${colors.green};
    border-radius: 4px;
  }
  &.warning {
    color: ${colors.red};
    font-weight: ${weight.medium};
  }
`

const Button = ({ kind, ...props }) => (
  <Main className={kind} {...props} />
);

Button.propTypes = {
  kind: PropTypes.string,
  children: PropTypes.any
};

Button.defaultProps = {
  kind: 'primary',
};

export default Button;
