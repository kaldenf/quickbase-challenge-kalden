import React, { useEffect, useState } from 'react'
import styled from "styled-components"
import { colors } from '../util/constants'
import PropTypes from 'prop-types'

const Wrap = styled.div`
  display: inline-block;
`

const Main = styled.div`
  width: 20px;
  height: 20px;
  border-radius: 3px;
  border: 1px solid #C4CAD0;
  background: white;
  position: relative;
  overflow: hidden;
  margin-right: 8px;
  cursor: pointer;
  user-select: none;
`
const Check = styled.div`
  width: 20px;
  height: 20px;
  background: ${colors.blue};
  display: flex;
  align-items: middle;
  & img {
    margin-left: 3px;
    width: 12px;
  }
`

const Checkbox = (props) => {
  const [selected, setSelected] = useState(props.selected)

  useEffect(() => {
    setSelected(props.selected)
  }, [props.detect])

  return (
    <Wrap {...props}>
      {selected ? (
        <Main onClick={() => setSelected(!selected)}>
          <Check><img src="/icon-checkmark-blue.svg" /></Check>
        </Main>
      ) : (
        <Main onClick={() => setSelected(!selected)} />
      )}
    </Wrap>
  )
}

Checkbox.propTypes = {
  selected: PropTypes.bool
};

export default Checkbox
