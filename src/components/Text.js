import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { weight } from '../util/constants'

const Main = styled.div`
  /* Kinds */
  &.h2 {
    font-size: 18px;
    font-weight: ${weight.medium};
  }

  /* Defaults */
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
    'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
    sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;

  /* Customizations */
  color: ${({ color }) => color};
  text-align: ${({ align }) => align};
  text-transform: ${({ transform }) => transform};
  opacity: ${({ opacity }) => opacity};
  font-weight: ${({ weight }) => weight};
  line-height: ${({ line }) => line || '1.4'};
  padding: ${({ padding }) => padding};
  font-size: ${({ size }) => size};

`;

const Text = ({ kind, className, ...props }) => <Main className={kind} {...props} as={kind}>{props.children}</Main>

Text.propTypes = {
  /** Prop to declare text kind, relating to HTML elements; accepts values in this list; if h1 is declared it will rendered an h1 element for SEO purposes, otherwise it will render a div */
  kind: PropTypes.oneOf([ 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p', 'span' ]),
  color: PropTypes.string,
  align: PropTypes.string,
  opacity: PropTypes.number,
  weight: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  height: PropTypes.string,
  padding: PropTypes.string,
};

/** @component */
export default Text;
