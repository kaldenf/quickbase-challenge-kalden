import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { colors, weight } from '../util/constants'

const Main = styled.input`
  padding: 10px 15px;
  width: 100%;
  border: 1px solid ${colors.gray_100};
  background: none;
  font-size: 16px;
  border-radius: 6px;
`

const Input = ({ kind, ...props }) => {
  const [value, setValue] = useState('')

  function updateField(e) {
    if(props.change) props.change(e)
    setValue(e.target.value)
  }

  useEffect(() => {
    setValue(props.initialvalue ? props.initialvalue : '')
  }, [props.detect])

  return <Main {...props} onChange={updateField} value={value} />
}

Input.propTypes = {
  kind: PropTypes.string
};

export default Input;
