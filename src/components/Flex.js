import styled from 'styled-components';
import PropTypes from 'prop-types';

const Flex = styled.div`
  width: 100%;
  display: flex;
  flex-direction: ${props => props.column && 'column'};
  justify-content: ${props => props.justify ? props.justify : 'space-between'};
  flex-wrap: ${({ flexWrap }) => flexWrap};
  align-items: ${props => (props.align ? props.align : 'center')};
  width: ${props => props.width && props.width};
  height: ${props => props.height && props.height};
  padding: ${props => props.padding && props.padding};
  margin: ${props => props.margin && props.margin};
  max-width: ${props => props.maxWidth && props.maxWidth};
  max-height: ${props => props.maxHeight && props.maxHeight};
`;

Flex.propTypes = {
  column: PropTypes.bool,
  justify: PropTypes.string,
  align: PropTypes.string,
  flexWrap: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  padding: PropTypes.string,
  margin: PropTypes.string,
  maxWidth: PropTypes.string,
};

Flex.defaultProps = {
  column: false,
  justify: 'flex-start',
  align: 'flex-start',
  flexWrap: 'nowrap',
};

export default Flex;
