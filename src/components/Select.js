import React, { useEffect, useState, useRef } from 'react'
import styled from "styled-components"
import Text from './Text'
import PropTypes from 'prop-types'

const Main = styled.div`
  background: white;
  border: 1px solid #C4CAD0;
  width: 100%;
  border-radius: 6px;
  height: 40px;
  position: relative;
  display: flex;
  align-items: center;
  padding: 0 32px 0 16px;
  cursor: pointer;
  margin: ${props => props.margin ? props.margin : '0'};
  max-width: ${props => props.width ? props.width : 'auto'};
  &:after {
    content: '';
    background: url('/icon-downarrow.svg');
    background-size: contain;
    width: 12px;
    height: 7px;
    position: absolute;
    top: 45%;
    right: 8px;
  }
  &:focus {
    box-shadow: 0 0px 8px rgba(0,0,0,0.25);
    outline: none;
  }
`

const List = styled.div`
  position: absolute;
  top: 95%;
  left: 0;
  background: white;
  border: 1px solid #ddd;
  width: 100%;
  z-index: 3;
`

const Item = styled.div`
  padding: 8px 16px;
  background: white;
  transition-duration: 0.3s;
  cursor: pointer;
  font-size: 13px;
  &:hover {
    background: #eee;
  }
`

const Select = (props) => {
  const [selected, setSelected] = useState()
  const [showMenu, setShowMenu] = useState(false)

  const node = useRef();

  const handleClick = e => {
    if (node.current.contains(e.target)) {
      return;
    }

    setShowMenu(false)
  };

  function select(item){
    setSelected(item)
    if(props.change) props.change(item)
  }

  useEffect(() => {
    setSelected(props.initialvalue ? props.initialvalue : props.options[0])
    // add when mounted
    document.addEventListener("mousedown", handleClick);
    // return function to be called when unmounted
    return () => {
      document.removeEventListener("mousedown", handleClick);
    };
  }, [props.detect]);

  return (
    <Main ref={node} {...props} onClick={() => setShowMenu(!showMenu)}>
      {selected && <Text>{selected.label}</Text>}

      {showMenu && (
        <List>
          {props.options && props.options.map((item, index) => (
            <Item key={index} onClick={() => select(item)}><Text>{item.label}</Text></Item>
          ))}
        </List>
      )}
    </Main>
  )
}

Select.propTypes = {
  initialvalue: PropTypes.object,
  options: PropTypes.array
};

export default Select
